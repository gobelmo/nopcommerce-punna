﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Web.PunnaExtend;

namespace Nop.Web
{
    public class PunnaTempleController : Controller
    {
        // GET: PunnaTemple
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult TempleList(string keyword)
        {
            using (var db = new punnaextendEntities())
            {
                var result = (from q in db.temples select q).ToList();
                result = String.IsNullOrEmpty(keyword) ? result : result.Where(x => x.templeName.Contains(keyword) ||
                                                                                                x.province.Contains(keyword) ||
                                                                                                x.district.Contains(keyword) ||
                                                                                                x.district.Contains(keyword) ||
                                                                                                x.subdistrict.Contains(keyword)).ToList();

                var temple = result.Select(c => new { value = string.Format("{0} - {1} - {2} - {3}", c.templeName, c.subdistrict , c.district, c.province) , data = string.Format("{0} - {1} - {2} - {3}", c.templeName, c.subdistrict, c.district, c.province) });
                return Json(temple, JsonRequestBehavior.AllowGet);
            }
                
        }
    }
}